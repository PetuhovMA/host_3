<?php

header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (!empty($_GET['save'])) {
      echo "<script type='text/javascript'>alert('Ваши данные в полной сохранности!');</script>";
    }
    include('form.php');
    exit();
}
  
  

try{
    
    $errors = FALSE;
    if (empty($_POST['fio'])) {
        print('Введите имя.<br/>');
        $errors = TRUE;
    }
    if (empty($_POST['email'])) {
        print('Введите почту.<br/>');
        $errors = TRUE;
    }
    if (empty($_POST['job'])) {
        print('Введите дату рождения.<br/>');
        $errors = TRUE;
    }
    if (empty($_POST['gender'])) {
        print('Укажите пол.<br/>');
        $errors = TRUE;
    }
    if (empty($_POST['sp-sp'])) {
        print('Введите процессию.<br/>');
        $errors = TRUE;
    }
    if (empty($_POST['_value'])) {
        print('Введите биографию.<br/>');
        $errors = TRUE;
    }
    if (empty($_POST['bio'])) {
        print('Введите биографию.<br/>');
        $errors = TRUE;
    }
    if (empty($_POST['galka'])) {
        print('Согласитесь с условиями.<br/>');
        $errors = TRUE;
    }

    if ($errors) {
        exit();
    }


    $conn = new PDO("mysql:host=localhost;dbname=u20989", 'u20989', '5299306', array(PDO::ATTR_PERSISTENT => true));

    $user = $conn->prepare("INSERT INTO Users SET fio = ?, email = ?, yob = ?, gender = ?, _value = ?, bio = ?");
    $user -> execute([$_POST['fio'], $_POST['email'], $_POST['job'], $_POST['gender'], $_POST['_value'], $_POST['bio']]);
    $id_user = $conn->lastInsertId();

    $foot = $conn->prepare("INSERT INTO Foots SET id_user = ?");
    $foot -> execute([$id_user]);
    $id_foot = $conn->lastInsertId();

    $music = implode(',',$_POST['sp-sp']);

    $music = $conn->prepare("INSERT INTO Music SET musicID = ?, music_names = ?");
    $music -> execute([$id_music, $music]);

    header("Location: ?save=1");
}
catch(PDOException $e){
    print('Error : ' . $e->getMessage());
    exit();
}
?>
