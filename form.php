<!DOCTYPE html>

<head>
  <link rel="stylesheet" href="style.css">
  <meta charset="utf-8">
  <title>Мать природа</title>
</head>
<body >
  <header>
  <div class="banner">
  <a href="#home"><img id="logo" src="images/totem.jpg" width="100" height="100"  alt="Логотип"/></a>

  <h1>Природа зовёт</h1>
</div>
</header>

<div class="container">
  <div class="forma">
  <h2 id="form">Ваша форма</h2>

  <form action="" method="POST">
    <ol>
      <li>ФИО: <input name="fio" placeholder="Введите ФИО" /> </li>
      <li>E-mail: <input name="email" placeholder="Введите email" type="email" /></li>
      <li>Дата рождения: <input name="yob" value="2012-12-21" type="date"/> </li>
      <li>
        Пол :
        <input type="radio" checked="checked" name="gender" value="man"/>Муж.
        <input type="radio" name="gender" value="woman"/>Жен.
        <input type="radio" name="gender" value="zver"/> Зверь
      </li>
      <li>
        Ваш размер обуви:
        <input type="radio" name="_value" value="1"/>39
        <input type="radio" name="_value" value="2"/>40
        <input type="radio" name="_value" value="3"/>41
        <input type="radio" checked="checked" name="_value" value="4"/>42
        <input type="radio" name="_value" value="5"/>50
      </li>
      <li>
        Любимая музыка:
        <select name="sp-sp[]" multiple="multiple" >
          <option value="rok">Рок</option>
          <option value="metall">Металл</option>
          <option value="pop">Поп</option>
          <option value="wonder_sounds">Звуки природы</option>
        </select>
      </li>
      <li>
        Биография :<textarea name="bio" placeholder="Введите свою (желательно, но не обязательно) биографию" ></textarea>
      </li>
      <li>
        <input type="checkbox" name="galka" />С условиями ознакомлена(ы)
      </li>
      <li>
        <input type="submit" value="Отправить" />
      </li>
    </ol>
  </form>
</div>
</div>
<footer>
<h2>Если природа зовёт, лучше не терпеть</h2>
</footer>

</body>
</html>
